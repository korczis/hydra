#! /usr/bin/env bash

apt-get install -y \
    apt-utils \
    autoconf \
    automake \
    build-essential \
    curl \
    dirmngr \
    fluxbox \
    fonts-ipafont-gothic \
    fonts-wqy-zenhei \
    fonts-thai-tlwg \
    fonts-kacst \
    fop \
    gdebi \
    git \
    htop \
    inotify-tools \
    locales \
    libffi-dev \
    libgl1-mesa-dev \
    libglu1-mesa-dev \
    libncurses5-dev \
    libpng-dev \
    libreadline-dev \
    libssl-dev \
    libssh-dev \
    libtool \
    libwxgtk3.0-dev \
    libxml2-dev \
    libxml2-utils \
    libxslt1-dev \
    libyaml-dev \
    mc \
    net-tools \
    procps \
    socat \
    software-properties-common \
    sudo \
    unixodbc-dev \
    vim \
    xorg \
    xserver-xorg \
    x11vnc \
    xvfb \
    xsltproc
