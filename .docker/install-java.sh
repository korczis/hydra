#! /usr/bin/env bash

echo oracle-java13-installer shared/accepted-oracle-license-v1-2 select true | /usr/bin/debconf-set-selections

add-apt-repository ppa:linuxuprising/java -y \
 && apt-get update -q \
 && apt-get install oracle-java13-installer -y \
 && apt-get clean

java -version