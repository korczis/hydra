// See https://github.com/checkly/puppeteer-examples

const puppeteer = require('puppeteer');

const WS_URL = "ws://localhost:4000/api/v1/browser/ws";

(async () => {
    const opts = {
        browserWSEndpoint: WS_URL
    };

    const browser = await puppeteer.connect(opts);

    const page = await browser.newPage();
    await page.setViewport({ width: 1280, height: 800 });
    await page.goto('https://www.nytimes.com/');
    await page.screenshot({ path: 'nytimes.png', fullPage: true });

    await browser.close();
})();
