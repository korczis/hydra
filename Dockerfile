FROM x11vnc/desktop

ENV DEBIAN_FRONTEND noninteractive

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

ENV DISPLAY :99

COPY .docker .docker

# install ubuntu packages
RUN .docker/apt-get-update.sh

RUN .docker/apt-get-upgrade.sh

RUN .docker/apt-get-install.sh

RUN .docker/install-java.sh

# set the locale
RUN locale-gen en_US.UTF-8

RUN groupadd -r pptruser && useradd -r -g pptruser -G audio,video,sudo pptruser \
    && mkdir -p /home/pptruser/Downloads \
    && chown -R pptruser:pptruser /home/pptruser

RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN mkdir ~/.vnc \
    && mkdir /tmp/.X11-unix \
    && chmod 1777 /tmp/.X11-unix \
    && chown root /tmp/.X11-unix

# Run everything after as non-privileged user.
USER pptruser

# install asdf and its plugins
# ASDF will only correctly install plugins into the home directory as of 0.7.6
# so .... Just go with it.
ENV ASDF_ROOT /home/pptruser/.asdf
ENV PATH "${ASDF_ROOT}/bin:${ASDF_ROOT}/shims:$PATH"
RUN git clone https://github.com/asdf-vm/asdf.git ${ASDF_ROOT} --branch v0.7.6  \
 && asdf plugin-add erlang https://github.com/asdf-vm/asdf-erlang \
 && asdf plugin-add elixir https://github.com/asdf-vm/asdf-elixir \
 && asdf plugin-add nodejs https://github.com/asdf-vm/asdf-nodejs \
 && ${ASDF_ROOT}/plugins/nodejs/bin/import-release-team-keyring

# install erlang
ENV ERLANG_VERSION 22.1
RUN asdf install erlang ${ERLANG_VERSION} \
 && asdf global erlang ${ERLANG_VERSION}

# install elixir
ENV ELIXIR_VERSION 1.9.4
RUN asdf install elixir ${ELIXIR_VERSION} \
 && asdf global elixir ${ELIXIR_VERSION}

# install nodejs
ENV NODEJS_VERSION 12.13.1
RUN asdf install nodejs ${NODEJS_VERSION} \
 && asdf global nodejs ${NODEJS_VERSION}

EXPOSE 4000
EXPOSE 4369
EXPOSE 5999

ARG MIX_ENV=test
ENV MIX_ENV=$MIX_ENV

ARG PORT=4000
ENV PORT=$PORT

ENV SHELL=/bin/bash

ARG MULTILOGIN_USER
ENV MULTILOGIN_USER=$MULTILOGIN_USER

ARG MULTILOGIN_PASS
ENV MULTILOGIN_PASS=$MULTILOGIN_PASS

# install local Elixir hex and rebar
RUN mix local.hex --force \
 && mix local.rebar --force

COPY . /home/pptruser/app

USER root

RUN chown -R pptruser /home/pptruser/app

# Run everything after as non-privileged user.
USER pptruser

WORKDIR /home/pptruser/app

RUN mix deps.get
RUN mix deps.compile
RUN mix compile

RUN mkdir tmp || true
RUN mix multilogin.download

# RUN mix test

#RUN mv ./node_modules ./node_modules.tmp || true
#RUN mv ./node_modules.tmp ./node_modules || true

RUN rm -rf apps/hydra_web/assets/node_modules || true

WORKDIR /home/pptruser/app/apps/hydra_web/assets

RUN npm install
RUN npm run deploy

WORKDIR /home/pptruser/app

CMD ["./start.sh"]
