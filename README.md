# Hydra

## Prerequisites

- [Docker](https://www.docker.com/)
- [Elixir](https://elixir-lang.org/)

## Getting started

**Clone repository**

```bash
# Clone repository using git protocol
$ git clone https://github.com/korczis/hydra.git
```

**Get elixir dependencies**

```bash
# Use mix to get required dependencies
$ mix deps.get
```

**Run PostgreSQL**

```bash
# Run PostgreSQL locally using docker
./run-postgres.sh
```

**Create persistence**

```bash
# Use ecto to create databases required
$ mix ecto.create
```

**Migrate persistence**

```bash
# Use ecto to migrate databases
$ mix ecto.migrate
```

## Building Application

```bash
# Compile application
$ mix compile
```

## Testing Application

Running the tests.

```bash
# Build and run test
$ mix test
```

Running the credo.

Credo is a static code analysis tool for elixir.

```bash
# Run credo
$ mix credo
```

Running the dialyzer

Mix tasks to simplify use of Dialyzer in Elixir projects.

```bash
# Run dialyzer
$ mix dialyzer