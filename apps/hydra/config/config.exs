# Since configuration is shared in umbrella projects, this file
# should only configure the :hydra application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

envar = fn name ->
  #
  # https://github.com/ueberauth/ueberauth_google/issues/40
  #
  # Detects whether Distillery is currently loaded, which is the behavior when building a release,
  # via mix release, which calls a Distillery task.
  #
  # If Distillery is loaded, then presumably the release will eventually be run with `REPLACE_OS_VARS`
  # defined, which allows the boot script to replace all values in `sys.config` within the release
  # with proper values from the environment. In these cases, emit name of the environment variable
  # wrapped in ${} so the script provided by Distillery can fix them up at boot time.
  #
  # Otherwise it is presumed that the config file is being evaluated outside of running a release.
  # This can happen, for example, during local development or testing. When this is the case,
  # since the configuration is not to be compiled into anything else, it is safe to invoke
  # `System.get_env/1` right away to get the desired value.
  #
  case List.keyfind(Application.loaded_applications(), :distillery, 0) do
    nil -> System.get_env(name)
    _ -> "${#{name}}"
  end
end

import_config "#{Mix.env()}.exs"

config :hydra, Hydra.Multilogin,
       username: envar.("MULTILOGIN_USER"),
       password: envar.("MULTILOGIN_PASS")

config :hydra, Hydra.Multilogin.Cli,
       cmd: "/Applications/multilogin.app/Contents/MacOS/cli.sh"

config :hydra, Hydra.Multilogin.Headless,
       cmd: "/Applications/multilogin.app/Contents/MacOS/headless.sh"

config :hydra, Hydra.Multilogin.ApiLocal,
       host: "http://127.0.0.1",
       port:  35_000,
       timeout:  60_000

config :hydra, Hydra.Multilogin.ApiRemote,
       host: "https://api.multiloginapp.com"

config :hydra, Mix.Tasks.Multilogin.Download,
      url: 'https://cdn-download.multiloginapp.com/multilogin/4.5.3/multilogin-4.5.3-linux_x86_64.zip',
      path: './tmp/multilogin-4.5.3-linux_x86_64.zip',
      dir: './tmp/multilogin'
