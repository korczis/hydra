defmodule Hydra.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    import Supervisor.Spec

    children = [
      # Browser supervisor
      supervisor(Hydra.Browser.Supervisor, [
        [name: Hydra.Browser.Supervisor]
      ]),

      # Multilogin supervisor
      supervisor(Hydra.Multilogin.Supervisor, [
        [name: Hydra.Multilogin.Supervisor]
      ])
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [
      strategy: :one_for_one,
      name: Hydra.Supervisor
    ]

    Supervisor.start_link(children, opts)
  end
end
