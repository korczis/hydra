defmodule Hydra.Multilogin.ApiLocal do
  @moduledoc """
  Wrapper of multilogin.com Local REST API.
  """

  use GenServer
  require Logger

  @timeout Application.get_env(:hydra, __MODULE__)[:timeout]

  # GenServer API

  def start_link(args \\ [], opts \\ []) do
    GenServer.start_link(__MODULE__, args, opts ++ [name: __MODULE__])
  end

  def init(init_arg) do
    {:ok, init_arg ++ [browsers: %{}]}
  end

  def handle_call_profile_start(profile_id, state, {:ok, res}) do
    {:ok, payload} = res.body |> Poison.decode()

    case payload do
      %{ "value" => ws, "status" => "OK" } ->
        browsers = Keyword.get(state, :browsers)

        uuid = UUID.uuid1()
        new_browser = %{
          # status: status,
          ws: ws,
          uuid: uuid,
          profile: profile_id
        }

        {:ok, pid} = Hydra.Multilogin.SocketWatchdog.start_link(ws, state)

        Logger.info("Monitoring #{inspect(pid)}")
        _ = Process.monitor(pid)

        new_browsers =  Map.put(browsers, uuid, new_browser)

        new_state = Keyword.merge(state, [browsers: new_browsers])

        {:reply, {:ok, new_browser}, new_state}
      %{"status" => "ERROR"} ->
        {:reply, {:error, "unknown error"}, state}
      other ->
        {:reply, {:unknown, other}, state}
    end
  end

  def handle_call_profile_start(_profile_id, state, {:error, %HTTPoison.Error{reason: reason}}) do
    GenServer.stop(Hydra.Multilogin.Headless, {:error, :not_responding})
    {:reply, {:error, reason}, state}
  end

  def handle_call({:start, profile_id}, _from, state) do
    url = "#{host()}:#{port()}/api/v1/profile/start?profileId=#{profile_id}&automation=true&puppeteer=true"
    :ok = Logger.info("Starting profile - #{url}")

    res = HTTPoison.get(url, [{"Accept", "application/json"}], [timeout: 50_000, recv_timeout: 50_000])
    handle_call_profile_start(profile_id, state, res)
  end

  def handle_call({:stop, profile_id}, _from, state) do
    url = "#{host()}:#{port()}/api/v1/profile/stop?profileId=#{profile_id}"
    :ok = Logger.info("Stopping profile - #{url}")

    {:ok, res} = HTTPoison.get(url, [{"Accept", "application/json"}])
    {:ok, payload} = res.body
                     |> Poison.decode()

    # new_state = Keyword.merge(state, [browsers: new_browsers])

    {:reply, {:ok, payload}, state}
  end

  def handle_call({:is_active, profile_id}, _from, state) do
    url = "#{host()}:#{port()}/api/v1/profile/active?profileId=#{profile_id}"
    :ok = Logger.info(url)

    {:ok, res} = HTTPoison.get(url)
    {:ok, payload} = res.body
                     |> Poison.decode()

    {:reply, {:ok, payload["value"]}, state}
  end

#  def monitor(pid) do
#    GenServer.cast(__MODULE__, {:monitor, pid})
#  end
#
#  def handle_cast({:monitor, pid}, state) do
#    Process.monitor(pid)
#    {:noreply, state}
#  end

  def handle_info({:DOWN, ref, _, pid, reason}, state) do
    Logger.info "Handled :DOWN message from ref: #{inspect(ref)}, object: #{inspect(pid)}, reason: #{inspect(reason)}"
    {:noreply, state}
  end

  def handle_info({:EXIT, pid, reason}, state) do
    Logger.info "Handled :EXIT message from object: #{inspect(pid)}, reason: #{inspect(reason)}"
    {:noreply, state}
  end

  # Public API

  def host do
    Application.get_env(:hydra, __MODULE__)[:host]
  end

  def is_active?(profile_id) do
    GenServer.call(__MODULE__, {:is_active, profile_id})
  end

  def port do
    Application.get_env(:hydra, __MODULE__)[:port]
  end

  def start(profile_id) do
    GenServer.call(__MODULE__, {:start, profile_id}, @timeout)
  end

  def stop(profile_id) do
    GenServer.call(__MODULE__, {:stop, profile_id})
  end
end
