defmodule Hydra.Multilogin.ApiRemote do
  @moduledoc """
  Wrapper of multilogin.com Remote REST API.
  """

  use GenServer
  require Logger

  alias Hydra.Multilogin.Cli

  # GenServer API

  def start_link(args \\ [], opts \\ []) do
    GenServer.start_link(__MODULE__, args, opts ++ [name: __MODULE__])
  end

  def init(init_arg) do
    {:ok, init_arg ++ [token: Cli.get_token()]}
  end

  def handle_call({:token}, _from, state) do
    token = state |> Keyword.get(:token)
    {:reply, {:ok, token}, state}
  end

  def handle_call({:token_renew}, _from, state) do
    token = Cli.get_token()
    {:reply, {:ok, token}, Keyword.merge(state,  [token: token])}
  end

  def handle_call({:profile_details, profile_id}, _from, state) do
    token = state |> Keyword.get(:token)
    {:ok, res} = HTTPoison.get("#{host()}/v1/profile/get-data?token=#{token}&profileId=#{profile_id}")

    {:ok, payload} = res.body
                     |> Poison.decode()

    {:reply, {:ok, payload}, state}
  end

  def handle_call({:profile_list}, _from, state) do
    token = state |> Keyword.get(:token)
    {:ok, res} = HTTPoison.get("#{host()}/v1/profile/list?token=#{token}")

    {:ok, payload} = res.body
                     |> Poison.decode()

    #     get_details = fn item ->
    #        :timer.sleep(1000)
    #        {:ok, res_details} = HTTPoison.get("https://api.multiloginapp.com/v1/profile/get-data?token=#{token()}&profileId=#{item["sid"]}")
    #        res_details.body |> Poison.decode()
    #     end

    {:reply, {:ok, payload["data"]}, state}
  end

  def handle_call({:tag_create, tag_name}, _from, state) do
    token = state |> Keyword.get(:token)
    {:ok, res} = HTTPoison.get("#{host()}/v1/tag/create?token=#{token}&name=#{tag_name}")

    {:ok, payload} = res.body
                     |> Poison.decode()

    {:reply, payload, state}
  end

  def handle_call({:tag_list}, _from, state) do
    token = state |> Keyword.get(:token)
    {:ok, res} = HTTPoison.get("#{host()}/v1/tag/list?token=#{token}")

    {:ok, payload} = res.body
                     |> Poison.decode()

    {:reply, {:ok, payload["data"]}, state}
  end

  def handle_call({:tag_remove, tag_id}, _from, state) do
    token = state |> Keyword.get(:token)
    {:ok, res} = HTTPoison.get("#{host()}/v1/tag/remove?token=#{token}&tagId=#{tag_id}")

    {:ok, payload} = res.body
                     |> Poison.decode()

    {:reply, {:ok, payload}, state}
  end

  def handle_call({:tag_rename, tag_id, new_name}, _from, state) do
    token = state |> Keyword.get(:token)

    {:ok, res} = HTTPoison.get("#{host()}/v1/tag/rename?token=#{token}&tagId=#{tag_id}&name=#{new_name}")

    {:ok, payload} = res.body
                     |> Poison.decode()

    {:reply, {:ok, payload}, state}
  end

  # Public API

  def host do
    Application.get_env(:hydra, __MODULE__)[:host]
  end

  def profile_details(profile_id) do
    GenServer.call(__MODULE__, {:profile_details, profile_id})
  end

  def profile_list() do
    GenServer.call(__MODULE__, {:profile_list})
  end

  def tag_crate(tag_name) do
    GenServer.call(__MODULE__, {:tag_crate, tag_name})
  end

  def tag_list() do
    GenServer.call(__MODULE__, {:tag_list})
  end

  def tag_remove(tag_id) do
    GenServer.call(__MODULE__, {:tag_remove, tag_id})
  end

  def tag_rename(tag_id, new_name) do
    GenServer.call(__MODULE__, {:tag_rename, tag_id, new_name})
  end

  def token() do
    GenServer.call(__MODULE__, {:token})
  end

  def token_renew() do
    GenServer.call(__MODULE__, {:token_renew})
  end
end