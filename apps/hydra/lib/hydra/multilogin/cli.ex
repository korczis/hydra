defmodule Hydra.Multilogin.Cli do
  @moduledoc """
  Wrapper of multilogin.com cli.sh CLI script.
  """

  use GenServer
  require Logger

  # GenServer API
  def start_link(args \\ [], opts \\ []) do
    GenServer.start_link(__MODULE__, args, opts ++ [name: __MODULE__])
  end

  def init(init_arg) do
    {:ok, init_arg}
  end

  # Public API

  def cmd do
    Application.get_env(:hydra, __MODULE__)[:cmd]
  end

  def get_token() do
    username = Application.get_env(:hydra, Hydra.Multilogin)[:username]
    password = Application.get_env(:hydra, Hydra.Multilogin)[:password]

#    cmd = System.cwd
#          |> Path.join(cmd())

    cmd = cmd()

    Logger.info("Getting token - #{cmd}")

    {output, 0} = System.cmd(cmd, ["-login", "-u", username, "-p", password])
    String.split(output, "\n", trim: true)
    |> List.last
  end
end
