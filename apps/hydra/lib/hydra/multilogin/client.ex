defmodule Hydra.Multilogin.Client do
  @moduledoc """
  Unified multilogin.com Client.
  """

  require Logger

  # Public API

  def profile_details(profile_id) do
    :ok = Logger.info("Getting profile details - #{profile_id}")
    Hydra.Multilogin.ApiRemote.profile_details(profile_id)
  end

  def profile_is_active?(profile) do
    :ok = Logger.info("Stopping profile #{profile}")
    Hydra.Multilogin.ApiLocal.is_active?(profile)
  end

  def profile_list() do
    :ok = Logger.info("Listing profiles")
    Hydra.Multilogin.ApiRemote.profile_list()
  end

  def profile_start() do
#    profiles = profile_list()
#    profile = Enum.random(profiles)
#    profile_sid = profile["sid"]

    profile_sid = "e69ddc5a-2e46-4393-afed-2c3d10472b45"

    :ok = Logger.info("Starting random profile - #{profile_sid}")

    Hydra.Multilogin.ApiLocal.start(profile_sid)
  end

  def profile_start(profile) do
    :ok = Logger.info("Starting profile #{profile}")
    Hydra.Multilogin.ApiLocal.start(profile)
  end

  def profile_stop(profile) do
    :ok = Logger.info("Stopping profile #{profile}")
    Hydra.Multilogin.ApiLocal.stop(profile)
  end

  def tag_list() do
    :ok = Logger.info("Listing tags")
    Hydra.Multilogin.ApiRemote.tag_list()
  end
end