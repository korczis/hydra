defmodule Hydra.Multilogin.Headless do
  @moduledoc """
  Wrapper of multilogin.com headless.sh CLI script.
  """

  use GenServer
  require Logger

  # GenServer API
  def start_link(args \\ [], opts \\ []) do
    GenServer.start_link(__MODULE__, args, opts ++ [name: __MODULE__])
  end

  def init(_args \\ []) do
    command = "#{cmd()} -port #{port()}"

    try do
      _ = System.cmd("killall", ["cmd()"])
    rescue
      _e -> Logger.info("killall #{cmd()} has failed")
    end

    try do
      _ = System.cmd("killall", ["./tmp/multilogin/multilogin"])
    rescue
      _e -> Logger.info("killall multilogin has failed")
    end

    :ok = Logger.info "Running command: #{command}"

    # port = Port.open({:spawn, command}, [:binary, :exit_status])
     port = Port.open({:spawn_executable, "./cli-wrapper.sh"}, [:binary, args: [cmd(), "-port", "#{port()}"]])

    Port.monitor(port)

    {:ok, %{lines: nil, exit_status: nil, port: port} }
  end

  def handle_info({:DOWN, _ref, :port, port, reason}, state) do
    Logger.info "Handled :DOWN message from port: #{inspect(port)} - #{inspect(reason)}"

    # {:noreply, state}

    {:stop, :normal, state}
  end

  # This callback handles data incoming from the command's STDOUT
  def handle_info({_port, {:data, output}}, state) do
    lines = String.split(output, "\n", trim: true)
    Enum.each(lines, fn line -> Logger.info(line) end)

    {:noreply, %{state | lines: lines}}
  end

  # This callback tells us when the process exits
  def handle_info({_port, {:exit_status, status}}, state) do
    :ok = Logger.info "External exit: :exit_status: #{status}"

    new_state = %{state | exit_status: status}
    {:noreply, new_state}
  end

  # no-op catch-all callback for unhandled messages
  def handle_info(_msg, state), do: {:noreply, state}

  # Public API

  def cmd do
    Application.get_env(:hydra, Hydra.Multilogin.Headless)[:cmd]
  end

  def port do
    Application.get_env(:hydra, Hydra.Multilogin.ApiLocal)[:port]
  end
end