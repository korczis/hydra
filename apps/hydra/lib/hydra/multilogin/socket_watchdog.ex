defmodule  Hydra.Multilogin.SocketWatchdog do
  use WebSockex

  require Logger

  def start_link(url, state) do
    Logger.info("Starting #{__MODULE__} - #{url}")
    WebSockex.start_link(url, __MODULE__, state)
  end

  def handle_info({:DOWN, ref, _, pid, reason}, state) do
    Logger.info "Handled :DOWN message from ref: #{inspect(ref)}, object: #{inspect(pid)}, reason: #{inspect(reason)}"
    {:noreply, state}
  end

  def handle_info({:EXIT, pid, reason}, state) do
    Logger.info "Handled :EXIT message from object: #{inspect(pid)}, reason: #{inspect(reason)}"
    {:noreply, state}
  end

  def handle_disconnect(_connection_status_map, state) do
    Logger.info "Handled disconnect"
    {:ok, state}
  end
end