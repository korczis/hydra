defmodule Hydra.Multilogin.Supervisor do
  @moduledoc """
  Root supervisor of multilogin.com related modules
  """

  use Supervisor
  require Logger

  def init(_init_arg) do
    children = [
      Hydra.Multilogin.Headless,

      Hydra.Multilogin.ApiLocal,
      Hydra.Multilogin.ApiRemote,

      Hydra.Multilogin.Cli
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end

  # GenServer API
  def start_link(args \\ [], opts \\ []) do
    Supervisor.start_link(__MODULE__, args, opts ++ [name: __MODULE__])
  end
end