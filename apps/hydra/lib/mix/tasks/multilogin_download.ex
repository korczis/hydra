defmodule Mix.Tasks.Multilogin.Download do
  @moduledoc """
  Task for downloading multilogin binaries, easily.
  """

  use Mix.Task
  require Logger

  def dir do
    Application.get_env(:hydra, Mix.Tasks.Multilogin.Download)[:dir]
  end

  def path do
    Application.get_env(:hydra, Mix.Tasks.Multilogin.Download)[:path]
  end

  def url do
    Application.get_env(:hydra, Mix.Tasks.Multilogin.Download)[:url]
  end

  @shortdoc "Download multilogin."
  def run(_) do
    {:ok, _started} = Application.ensure_all_started(:inets)
    {:ok, _started} = Application.ensure_all_started(:ssl)

    {:ok, _} = case File.exists?(path()) do
      true ->
        :ok = Logger.info("Already downloaded #{url()} -> #{path()}")
        {:ok, :already_downloaded}
      false ->
        :ok = Logger.info("Downloading #{url()} -> #{path()}")
        {:ok, :saved_to_file} = :httpc.request(:get, {url(), []}, [], [stream: path()])
    end

    {:ok, _} = case File.exists?(dir()) do
      true ->
        :ok = Logger.info("Already extracted #{dir()}")
        {:ok, :already_extracted}
      false ->
        :ok = Logger.info("Extracting #{path()} -> #{dir()}")
        System.cmd("unzip", ["#{path()}", "-d", "#{dir()}"])
        {:ok, :extracted}
    end

    :ok = Logger.info("Making binaries executable")

    System.cmd("chmod", ["+x", "#{dir()}/cli.sh"])
    System.cmd("chmod", ["+x", "#{dir()}/headless.sh"])
    System.cmd("chmod", ["+x", "#{dir()}/multilogin"])
    {:ok, path()}
  end
end