defmodule Hydra.MixProject do
  use Mix.Project

  def project do
    [
      app: :hydra,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.5",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Hydra.Application, []},
      extra_applications: [
        :inets,
        :logger,
        :runtime_tools,
        :ssl,
        :httpoison,
        :websockex
      ]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:castore, "~> 0.1.0"},
      {:mint, "~> 1.0"},
      {:httpoison, "~> 1.6"},
      {:poison, "~> 4.0"},
      {:execjs, "~> 2.0"},
      {:toolshed, "~> 0.2.11"},
      {:uuid, "~> 1.1" },
      {:websockex, "~> 0.4"},
      # {:chroxy, git: "https://github.com/korczis/chroxy.git"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to create, migrate and run the seeds file at once:
  #
  #     $ mix ecto.setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    []
  end
end
