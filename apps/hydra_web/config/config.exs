# Since configuration is shared in umbrella projects, this file
# should only configure the :hydra_web application itself
# and only for organization purposes. All other config goes to
# the umbrella root.
use Mix.Config

# General application configuration
config :hydra_web,
       generators: [context_app: :hydra]

# Configures the endpoint
config :hydra_web, HydraWeb.Endpoint,
       url: [host: "localhost"],
       secret_key_base: "xM3JUgmXSf01RYsT0aes2+mjdcEUCVKoXbzviRRzN4mJnE40yc4EeDLXlKKc8YI9",
       render_errors: [view: HydraWeb.ErrorView, accepts: ~w(html json)],
       pubsub: [name: HydraWeb.PubSub, adapter: Phoenix.PubSub.PG2],
       http: [
         dispatch: [
           {:_, [
             {"/api/v1/browser/ws", HydraWeb.RawSocket.Cowboy, []},
             {:_, Phoenix.Endpoint.Cowboy2Handler, {HydraWeb.Endpoint, []}}
           ]
           }
         ]
       ]


# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
