defmodule HydraWeb.Api.V1.BrowserController do
  @moduledoc """
  Implementation independent browser controller.
  """

  use HydraWeb, :controller

  alias Hydra.Multilogin.Client

  require Logger

  def start(conn, _params) do
    case  Client.profile_start() do
      {:ok, res} ->
        json(conn, %{
          data: res
        })
      {:error, reason} ->
        json(conn, %{
          error: %{
            reason: reason
          }
        })
    end
  end

  def stop(conn, params) do
    {:ok, res} = Client.profile_stop(params["profile"])
    json(conn, %{
      data: res
    })
  end
end