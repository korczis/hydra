defmodule HydraWeb.Api.V1.Multilogin.ProfileController do
  @moduledoc """
  Multilogin profile management controller.
  """

  use HydraWeb, :controller

  alias Hydra.Multilogin.Client

  def index(conn, _params) do
    {:ok, res} = Client.profile_list()
    json(conn, %{
      data: res
    })
  end

  def show(conn, %{"id" => id}) do
    {:ok, item} = Client.profile_details(id)
    {:ok, active} = Client.profile_is_active?(id)
    json(conn, %{
      data: %{
        profile: item,
        active: active
      }
    })
  end

  def start(conn, %{"id" => id}) do
    case  Client.profile_start(id) do
      {:ok, res} ->
        json(conn, %{
          data: res
        })
      {:error, reason} ->
        json(conn, %{
          error: %{
            reason: reason
          }
        })
    end
  end

  def stop(conn, %{"id" => id}) do
    {:ok, item} = Client.profile_stop(id)
    json(conn, %{
      data: item
    })
  end
end