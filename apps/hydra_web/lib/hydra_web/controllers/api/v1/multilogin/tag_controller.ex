defmodule HydraWeb.Api.V1.Multilogin.TagController do
  @moduledoc """
  Multilogin tag/group management controller.
  """

  use HydraWeb, :controller

  alias Hydra.Multilogin.Client

  def index(conn, _params) do
    {:ok, items} = Client.tag_list()
    json(conn, %{
      data: items
    })
  end
end