defmodule HydraWeb.RawSocket.Websockex do
  require Logger

  def start_link(state) do
    Logger.info("start_link - #{inspect(state)}")

    url = state[:url]
    WebSockex.start_link(url, __MODULE__, state)
  end

  def handle_connect(conn, state) do
    {:ok, state}
  end

  def handle_frame(frame, state) do
    Logger.info("handle_frame - #{inspect(frame)}, #{inspect(state)}")

    Logger.info("handle_frame - before getting target")
    target = state[:target]
    Logger.info("handle_frame - got target: #{inspect(target)}")

    Logger.info("handle_frame - target: #{inspect(target)}, state: #{inspect(state)}")

    res = send(target, frame)
    Logger.info("send - #{inspect(res)}")

    Logger.info("handle_frame, response sent")
    {:ok, state}
  end
end

defmodule HydraWeb.RawSocket.Cowboy do
  @behaviour :cowboy_websocket

  require Logger

  def init(req, state) do
    Logger.info("init #{inspect(req)}, #{inspect(state)}")

    {:ok, res} = Hydra.Multilogin.Client.profile_start()

    {:cowboy_websocket, req, state ++ [ws: res.ws]}
  end


  # Everything else
  def handle_call(other, from, state) do
    Logger.info("handle_call #{inspect(other)}, #{inspect(from)}, #{state}")
    {:ok, state}
  end
  # Client sends message to server.
  def handle_cast({:websocket_request, message}, %{gun_pid: gun_pid} = state) do
    Logger.info("handle_cast #{inspect(message)}, #{inspect(gun_pid)}, #{state}")

    {:noreply, state}
  end

  def websocket_init(state) do
    Logger.info("websocket_init, state: #{inspect(state)}")

    {:ok, pid} = HydraWeb.RawSocket.Websockex.start_link([url: state[:ws], target: self(), debug: [:trace]])

    {:ok, state ++ [pid: pid]}
  end

  def websocket_handle({:text, message}, state) do
    Logger.info("websocket_handle (:text) #{inspect(message)}, #{inspect(state)}")

    {:ok, payload} = Poison.decode(message)

    case Map.fetch(payload, "method") do
      {:ok, _} ->
        Logger.info("Has frame")
        res = WebSockex.send_frame(state[:pid], {:text, message})
        IO.puts("Websockex.send_frame result #{inspect(res)}")
        {:ok, state}
      _ ->
        {:reply, {:text, payload}, state}
    end
  end

  def websocket_handle({:reply, message}, state) do
    Logger.info("websocket_handle (:reply) #{inspect(message)}, #{inspect(state)}")

    {:reply, {:text, message}, state}
  end

  def websocket_handle(whatev, state) do
    Logger.info("websocket_handle catchall #{inspect(whatev)}, #{inspect(state)}")
    {:reply, whatev, state}
  end

  def websocket_info(msg, state) do
    Logger.info("websocket_info #{inspect(msg)}, #{inspect(state)}")
    {:reply, msg, state}
  end
end
