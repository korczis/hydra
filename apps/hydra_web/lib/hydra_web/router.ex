defmodule HydraWeb.Router do
  use HydraWeb, :router

  # alias HydraWeb.Api.V1.Multilogin.ProfileController
  # alias HydraWeb.Api.V1.Multilogin.TagController

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", HydraWeb do
    pipe_through :browser

    get "/", PageController, :index
  end

  # Other scopes may use custom stacks.
  scope "/api", HydraWeb do
    pipe_through :api

    scope "/v1", Api.V1, as: :v1 do
      scope "/browser" do
        post "/start", BrowserController, :start
        post "/stop", BrowserController, :stop
      end

      scope "/multilogin", Multilogin do
        # Profile related routes
        resources "/profiles", ProfileController, only: [:index, :show]

        post "/profiles/:id/start", ProfileController, :start
        post "/profiles/:id/stop", ProfileController, :stop

        # Tag related routes
        resources "/tags", TagController, only: [:index]
      end
    end
  end
end
