#! /usr/bin/env bash

# -p 4369:4369
docker run --cap-add=SYS_ADMIN --privileged -p 4000:4000  -p 5999:5999 -p 6000:6000 --memory=2g --name hydra hydra:latest

