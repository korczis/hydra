defmodule Hydra.Umbrella.MixProject do
  use Mix.Project

  def project do
    [
      apps_path: "apps",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      dialyzer: [ flags: ["-Wunmatched_returns", :error_handling, :underspecs], plt_add_apps: [:mix]],
      version: "0.1.0",
      elixir: "~> 1.9",
      releases: [
        hydra_umbrella: [
          applications: [
            hydra: :permanent,
            hydra_web: :permanent
          ]
        ]
      ]
    ]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options.
  #
  # Dependencies listed here are available only for this project
  # and cannot be accessed from applications inside the apps folder
  defp deps do
    [
      {:credo, "~> 1.1.0", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 0.5", only: [:dev], runtime: false},
      {:distillery, "~> 2.1"},
      {:ex_doc, "~> 0.21"},
      {:libcluster, "~> 3.1"}
    ]
  end
end
