#!/usr/bin/env bash

# port forwarding as chrome is unable to spawn remote debugging address on 0.0.0.0 in headful mode <- issue in docker
#socat -d tcp-listen:9223,reuseaddr,fork tcp:127.0.0.1:9222 &
#socat -d tcp-listen:9225,reuseaddr,fork tcp:127.0.0.1:9224 &
#socat -d tcp-listen:9227,reuseaddr,fork tcp:127.0.0.1:9226 &
#socat -d tcp-listen:9229,reuseaddr,fork tcp:127.0.0.1:9228 &
#socat -d tcp-listen:9231,reuseaddr,fork tcp:127.0.0.1:9230 &

export DISPLAY_NUM=99

# start desktop on $DISPLAY for VNC
# export DISPLAY=:$DISPLAY_NUM

Xvfb $DISPLAY -screen 0 1280x800x24 -ac -nolisten tcp -dpi 96 +extension RANDR &
sleep 3
echo "xvfb started"

/usr/bin/fluxbox -display $DISPLAY -screen 0 &
sleep 3
echo "fluxbox started"

x11vnc -passwd 123 -display $DISPLAY -N -forever -shared -quiet &
echo "x11vnc started"
sleep 3

mix phx.server

